from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from .models import (
    YurtType,
    Yurt,
    OrderYurts,
    YurtServices,
    Hotel,
    HotelRoomType,
    HotelRoom,
    HotelServices,
    OrderHotelRooms,
    Album,
    Images,
    News,
)


class YurtTypeSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        return YurtType.objects.create(**validated_data)

    class Meta:
        model = YurtType
        fields = ['name']


class ImgSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        return Images.objects.create(**validated_data)

    class Meta:
        model = Images
        fields = "__all__"


class AlbSerializer(serializers.ModelSerializer):
    images = ImgSerializer(many=True)

    class Meta:
        model = Album
        fields = '__all__'

    def create(self, validated_data):
        return Album.objects.create(**validated_data)


class YurtSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        return Yurt.objects.create(**validated_data)

    album = AlbSerializer(read_only=True)

    class Meta:
        model = Yurt
        fields = [
            'type',
            'location',
            'price',
            'album'
        ]


class OrderYurtsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderYurts
        fields = '__all__'
        read_only_fields = ['clientId']

    def create(self, validated_data):
        user = self.context['request'].user
        new_yurt_order = OrderYurts.objects.create(
            clientId=user,
            yurtId=validated_data['yurtId'],
            reservationStartDateTime=validated_data['reservationStartDateTime'],
            reservationEndDateTime=validated_data['reservationEndDateTime'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            status=validated_data['status'],
        )
        new_yurt_order.services.set(validated_data['services'])

        return new_yurt_order


class YurtServiceSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        return YurtServices.objects.create(**validated_data)

    class Meta:
        model = YurtServices
        fields = [
            'name',
            'price',
            'description'
        ]


class HotelSerializer(serializers.ModelSerializer):
    album = AlbSerializer(read_only=True)

    def create(self, validated_data):
        return Hotel.objects.create(**validated_data)

    class Meta:
        model = Hotel
        fields = ['name', 'album']


class HotelRoomTypeSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        return HotelRoomType.objects.create(**validated_data)

    class Meta:
        model = HotelRoomType
        fields = [
            'name',
            'price_per_day'
        ]


class HotelRoomSerializer(serializers.ModelSerializer):
    album = AlbSerializer(read_only=True)



    def create(self, validated_data):
        return HotelRoom.objects.create(**validated_data)

    class Meta:
        model = HotelRoom
        fields = [
            'room_number',
            'hotel_room_typeId',
            'hotelId',
            'description',
            'album',
        ]


class HotelServiceSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        return HotelServices.objects.create(**validated_data)

    class Meta:
        model = HotelServices
        fields = [
            'name',
            'price',
            'description',
            'hotelId'
        ]


class OrderHotelRoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderHotelRooms
        fields = '__all__'
        read_only_fields = ['clientId']

    def create(self, validated_data):
        user = self.context['request'].user
        new_room_order = OrderHotelRooms.objects.create(
            clientId=user,
            roomId=validated_data['roomId'],
            reservationStartDate=validated_data['reservationStartDate'],
            reservationEndDate=validated_data['reservationEndDate'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            status=validated_data['status']
        )
        new_room_order.services.set(validated_data['services'])

        return new_room_order


class GeneralOrderYurtsSerializer(serializers.ModelSerializer):
    clientId = serializers.StringRelatedField(read_only=True)
    services = YurtServiceSerializer(many=True)

    class Meta:
        model = OrderYurts
        fields = '__all__'


class GeneralOrderRoomsSerializer(serializers.ModelSerializer):
    clientId = serializers.StringRelatedField(read_only=True)
    services = HotelServiceSerializer(many=True)

    class Meta:
        model = OrderHotelRooms
        fields = '__all__'


class NewsSerializer(serializers.ModelSerializer):
    album = AlbSerializer(read_only=True)

    class Meta:
        model = News
        fields = ['name', 'title', 'text', 'album', 'status']
