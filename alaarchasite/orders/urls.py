from django.urls import path, include
from orders.views import (
    YurtTypeAPIList,
    GeneralOrderYurtsAPIView,
    YurtOrderAPICreate,
    YurtOrderAPIRetrieve,
    YurtOrderAPIUpdate,
    YurtOrderAPIDelete,
    YurtView,
    GeneralHotelRoomOrderAPIView,
    HotelRoomOrderAPICreate,
    HotelRoomOrderAPIRetrieve,
    HotelRoomOrderAPIUpdate,
    HotelRoomOrderAPIDelete,
    ImgAPICreate,
    NewsAPIView,
    NewsListAPIView,
    NewsAPICreate,
    NewsAPIUpdate,
    NewsAPIDelete,
)

urlpatterns = [
    path('admin/show_general_yurts_orders/', GeneralOrderYurtsAPIView.as_view()),
    path('admin/show_general_rooms_orders/', GeneralHotelRoomOrderAPIView.as_view()),


    path('yurt/', include([
        path('create/', YurtOrderAPICreate.as_view()),
        path('', YurtView.as_view({'get': 'list'})),
        path('<int:pk>', include([
            path('', YurtOrderAPIRetrieve.as_view()),
            path('update/', YurtOrderAPIUpdate.as_view()),
            path('delete/', YurtOrderAPIDelete.as_view()),
            path('image/', ImgAPICreate.as_view()),
        ]))
    ])),

    path('news/', include([
        path('', NewsListAPIView.as_view()),
        path('<int:pk>', include([
            path('', NewsAPIView.as_view()),
            path('update/', NewsAPIUpdate.as_view()),
            path('delete/', NewsAPIDelete.as_view()),
        ])),
        path('create/', NewsAPICreate.as_view())
    ])),


    path('hotel_room/create/', HotelRoomOrderAPICreate.as_view()),
    path('hotel_room/detail/<int:pk>/', HotelRoomOrderAPIRetrieve.as_view()),
    path('hotel_room/update/<int:pk>/', HotelRoomOrderAPIUpdate.as_view()),
    path('hotel_room/delete/<int:pk>/', HotelRoomOrderAPIDelete.as_view()),


    path("yurt_type/", YurtTypeAPIList.as_view())
]
