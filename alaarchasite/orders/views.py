from django.shortcuts import render
from rest_framework import viewsets, generics, serializers
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated, IsAdminUser
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND
from rest_framework.views import APIView
from rest_framework.response import Response

from .permissions import (
    IsAdminOrReadOnly,
    IsOwnerOrAdmin,
    IsOwnerOrReadOnly,
    IsOwner
)

from .models import (
    YurtType,
    Yurt,
    OrderYurts,
    YurtServices,
    Hotel,
    HotelRoomType,
    HotelRoom,
    HotelServices,
    OrderHotelRooms,
    News,
)
from .serializers import (
    YurtTypeSerializer,
    YurtSerializer,
    OrderYurtsSerializer,
    YurtServiceSerializer,
    HotelSerializer,
    HotelRoomTypeSerializer,
    HotelRoomSerializer,
    HotelServiceSerializer,
    OrderHotelRoomSerializer,
    GeneralOrderYurtsSerializer,
    GeneralOrderRoomsSerializer,
    ImgSerializer,
    NewsSerializer,
)


class ImgAPICreate(generics.CreateAPIView):
    serializers = ImgSerializer


""" -------------/ Yurts Order Views / ------------- """


class GeneralOrderYurtsAPIView(generics.ListAPIView):
    queryset = OrderYurts.objects.all()
    serializer_class = GeneralOrderYurtsSerializer


class YurtOrderAPICreate(generics.CreateAPIView):
    queryset = OrderYurts.objects.all()
    serializer_class = OrderYurtsSerializer
    permission_classes = [IsAuthenticated]


class YurtOrderAPIRetrieve(generics.RetrieveAPIView):
    queryset = OrderYurts.objects.all()
    serializer_class = GeneralOrderYurtsSerializer
    permission_classes = [IsOwner]


class YurtOrderAPIUpdate(generics.UpdateAPIView):
    queryset = OrderYurts.objects.all()
    serializer_class = OrderYurtsSerializer
    permission_classes = [IsOwner]


class YurtOrderAPIDelete(generics.DestroyAPIView):
    queryset = OrderYurts.objects.all()
    serializer_class = OrderYurtsSerializer
    permission_classes = [IsOwner]


""" -------------/ Order Hotel Rooms Views / ------------- """


class GeneralHotelRoomOrderAPIView(generics.ListAPIView):
    queryset = OrderHotelRooms.objects.all()
    serializer_class = GeneralOrderRoomsSerializer


class HotelRoomOrderAPICreate(generics.CreateAPIView):
    queryset = OrderHotelRooms.objects.all()
    serializer_class = OrderHotelRoomSerializer
    permission_classes = [IsAuthenticated]


class HotelRoomOrderAPIRetrieve(generics.RetrieveAPIView):
    queryset = OrderHotelRooms.objects.all()
    serializer_class = OrderHotelRoomSerializer
    permission_classes = [IsOwner]


class HotelRoomOrderAPIUpdate(generics.UpdateAPIView):
    queryset = OrderHotelRooms.objects.all()
    serializer_class = OrderYurtsSerializer
    permission_classes = [IsOwner]


class HotelRoomOrderAPIDelete(generics.DestroyAPIView):
    queryset = OrderHotelRooms.objects.all()
    serializer_class = OrderYurtsSerializer
    permission_classes = [IsOwner]


""" -------------/ Yurt Type Views / ------------- """


class YurtTypeAPIList(generics.ListCreateAPIView):
    queryset = YurtType.objects.all()
    serializer_class = YurtTypeSerializer
    permission_classes = [IsAdminOrReadOnly]


class YurtTypeAPIUpdate(generics.UpdateAPIView):
    queryset = YurtType.objects.all()
    serializer_class = YurtTypeSerializer
    permission_classes = [IsAdminOrReadOnly]


class YurtTypeAPIDestroy(generics.RetrieveDestroyAPIView):
    queryset = YurtType.objects.all()
    serializer_class = YurtTypeSerializer
    permission_classes = [IsAdminOrReadOnly]


class YurtTypeView(viewsets.ModelViewSet):
    queryset = YurtType.objects.all()
    serializer_class = YurtTypeSerializer


class YurtView(viewsets.ModelViewSet):
    queryset = Yurt.objects.all()
    serializer_class = YurtSerializer


class OrderYurtView(viewsets.ModelViewSet):
    queryset = OrderYurts.objects.all()
    serializer_class = OrderYurtsSerializer


class YurtServiceView(viewsets.ModelViewSet):
    queryset = YurtServices.objects.all()
    serializer_class = YurtServiceSerializer


class HotelView(viewsets.ModelViewSet):
    queryset = Hotel.objects.all()
    serializer_class = HotelSerializer


class HotelRoomTypeView(viewsets.ModelViewSet):
    queryset = HotelRoomType.objects.all()
    serializer_class = HotelRoomTypeSerializer


class HotelRoomView(viewsets.ModelViewSet):
    queryset = HotelRoom.objects.all()
    serializer_class = HotelRoomSerializer


class HotelServiceView(viewsets.ModelViewSet):
    queryset = HotelServices.objects.all()
    serializer_class = HotelServiceSerializer


class OrderHotelRoomView(viewsets.ModelViewSet):
    queryset = OrderHotelRooms.objects.all()
    serializer_class = OrderHotelRoomSerializer


""" -------------/ News Views / ------------- """


class NewsListAPIView(generics.ListCreateAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer


class NewsAPIView(generics.RetrieveAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer


class NewsAPICreate(generics.CreateAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    permission_classes = [IsAdminUser]


class NewsAPIUpdate(generics.UpdateAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    permission_classes = [IsAdminUser]


class NewsAPIDelete(generics.DestroyAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    permission_classes = [IsAdminUser]
