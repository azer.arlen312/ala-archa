from rest_framework import generics, viewsets

from .models import CustomUser
from .serializers import CustomUserSerializer


class UsersView(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer


