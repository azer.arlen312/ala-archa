from rest_framework import serializers
from .models import CustomUser


class CustomUserSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        user = CustomUser.objects.create_user(**validated_data)

        return user

    class Meta:
        model = CustomUser
        fields = ["email", "password", "phone_number"]
        extra_kwargs = {'password': {'write_only': True}}






